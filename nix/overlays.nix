{ inputs }:
let
  customHaskellPackages = self: super: {
    haskellPackages = super.haskellPackages.override {
      overrides =
        hself: hsuper:
        let
          dontCheck = super.haskell.lib.dontCheck;
          dontHaddock = super.haskell.lib.dontHaddock;
          doJailbreak = super.haskell.lib.compose.doJailbreak;

          comdirect-api-src = self.nix-gitignore.gitignoreSource [
            "*.git"
            "dist"
            "dist-newstyle"
          ] ../.;
          comdirect-api = dontHaddock (hself.callCabal2nix "comdirect-api" comdirect-api-src { });
        in
        {
          # We add ourselves to the set of haskellPackages.
          inherit comdirect-api;
        };
    };
  };
in
[
  # (final: prev: { haskellPackages = prev.haskell.packages.ghc98; })
  customHaskellPackages
]
