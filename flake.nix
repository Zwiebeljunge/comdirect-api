{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };
  outputs =
    inputs@{ self, flake-utils, ... }:
    flake-utils.lib.eachDefaultSystem (
      system:
      let
        pkgs = import ./nix/pkgs.nix { inherit inputs system; };
        packageName = "comdirect-api";
      in
      {
        packages = {
          ${packageName} = pkgs.haskellPackages.comdirect-api;
          default = pkgs.haskell.lib.justStaticExecutables self.packages.${system}.${packageName};
        };

        devShells = {
          default = pkgs.haskellPackages.shellFor {
            packages = p: [ p.comdirect-api ];
            nativeBuildInputs = with pkgs; [
              haskellPackages.cabal-install
              haskellPackages.ghc
              haskellPackages.hlint
              haskellPackages.ghcid
              haskellPackages.haskell-language-server
              haskellPackages.fourmolu
              haskellPackages.profiteur
            ];
          };
        };

        overlays = final: prev: {
          haskellPackages = prev.haskellPackages // {
            ${packageName} = pkgs.haskell.lib.justStaticExecutables self.packages.${system}.${packageName};
          };
        };
      }
    );
}
