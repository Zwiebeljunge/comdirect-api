{-# LANGUAGE DataKinds #-}
{-# LANGUAGE NumericUnderscores #-}
{-# LANGUAGE OverloadedStrings #-}

module Comdirect.Authentication
  ( toAuthorizationParameter,
    generateSecondaryAccessRefreshTokens,
    -- invalidateAccessRefreshToken,
    generateNewAccessRefreshTokens,
    oauthSetup,
    activateSessionTAN,
    secondaryAccessRefreshTokenRequest,
  )
where

import Comdirect.Types
import Comdirect.Utils
import Control.Concurrent (threadDelay)
import qualified Data.ByteString as BS (writeFile)
import qualified Data.ByteString.Base64 as B64 (decode)
import Data.ByteString.Char8 (ByteString, pack)
import qualified Data.ByteString.Lazy as LBS (fromStrict)
import Data.Maybe (fromJust, mapMaybe)
import Network.HTTP.Simple
import Polysemy
import qualified Polysemy.Error as Error

-- | Make an initial request for `AccessRefreshToken`
tokenRequest ::
  ( Members
      '[ HttpRequest,
         Error.Error ComdirectError
       ]
      r
  ) =>
  TokenRequestBody Password ->
  Sem r (AccessRefreshToken Password)
tokenRequest tokenRequestBody = do
  let req =
        setRequestHeaders [("accept", "application/json"), ("content-type", "application/x-www-form-urlencoded")] $
          setRequestBodyURLEncoded (tokenRequestParameters tokenRequestBody) $
            parseRequest_ "POST https://api.comdirect.de/oauth/token"
  request req >>= decodeJSON . getResponseBody

-- | Format `AccessRefreshToken` as header parameter
toAuthorizationParameter :: AccessRefreshToken g -> ByteString
toAuthorizationParameter (AccessRefreshToken accessT tokenT _ _ _) = pack $ tokenT <> " " <> accessT

sessionStatusRequest ::
  ( Members
      '[ HttpRequest,
         ClientRequest,
         Error.Error ComdirectError
       ]
      r
  ) =>
  AccessRefreshToken Password ->
  Sem r [SessionObject]
sessionStatusRequest accessRefreshToken = do
  clientRequestId <- getClientRequestId
  let req =
        setRequestHeaders
          [ ("accept", "application/json"),
            ("Authorization", toAuthorizationParameter accessRefreshToken),
            ("x-http-request-info", strictEncode clientRequestId)
          ]
          $ parseRequest_ "GET https://api.comdirect.de/api/session/clients/user/v1/sessions"
  request req >>= decodeJSON . getResponseBody

sessionTanRequest ::
  ( Members
      '[ Embed IO,
         HttpRequest,
         ClientRequest,
         Error.Error ComdirectError
       ]
      r
  ) =>
  AccessRefreshToken Password ->
  SessionObject ->
  Sem r TANChallenge
sessionTanRequest accessRefreshToken sessObj = do
  clientRequestId <- getClientRequestId
  let req =
        setRequestHeaders
          [ ("accept", "application/json"),
            ("content-type", "application/json"),
            ("Authorization", toAuthorizationParameter accessRefreshToken),
            ("x-http-request-info", strictEncode clientRequestId)
          ]
          $ setRequestBodyJSON (mkSession sessObj)
          $ parseRequest_ ("POST https://api.comdirect.de/api/session/clients/user/v1/sessions/" <> identifier sessObj <> "/validate")
  resp@(TANChallenge (ChallengeId challId) tanType chall _) <- request req >>= decodeJSON . LBS.fromStrict . head . getResponseHeader "x-once-authentication-info"
  case tanType of
    P_TAN -> do
      let filename = show challId <> "_tan.png"
      case B64.decode $ pack $ fromJust chall of
        Right pngContent -> embed $ BS.writeFile filename pngContent
        _ -> return ()
    _ -> return ()
  return resp

-- | Activate the session with the tan for the given `TANChallenge`
activateSessionTAN ::
  ( Members
      '[ HttpRequest,
         ClientRequest,
         Error.Error ComdirectError
       ]
      r
  ) =>
  TANType ->
  AccessRefreshToken Password ->
  SessionObject ->
  TANChallenge ->
  Maybe Int ->
  Sem r SessionObject
activateSessionTAN _tanType accessRefreshToken sessObj tanChallenge mTan = do
  clientRequestId <- getClientRequestId
  let req =
        setRequestHeaders
          ( [ ("accept", "application/json"),
              ("content-type", "application/json"),
              ("Authorization", toAuthorizationParameter accessRefreshToken),
              ("x-http-request-info", strictEncode clientRequestId),
              ("x-once-authentication-info", strictEncode $ challengeId tanChallenge)
            ]
              <> mapMaybe (fmap ((,) "x-once-authentication" . pack . show)) [mTan]
          )
          $ setRequestBodyJSON (mkSession sessObj)
          $ parseRequest_ ("PATCH https://api.comdirect.de/api/session/clients/user/v1/sessions/" <> identifier sessObj)
  request req >>= decodeJSON . getResponseBody

-- | Initialize oauth and request `TANCallenge`
oauthSetup ::
  ( Members
      '[ Embed IO,
         HttpRequest,
         ClientRequest,
         Error.Error ComdirectError
       ]
      r
  ) =>
  ComdirectConfig ->
  Sem r (AccessRefreshToken Password, SessionObject, TANChallenge)
oauthSetup comdConf = do
  accRefTok <- tokenRequest (mkTokenRequestBody comdConf :: TokenRequestBody Password)
  sessObjs <- sessionStatusRequest accRefTok
  case sessObjs of
    [] -> Error.throw $ ComdirectError "No session object found" Nothing
    (_ : _ : _) -> Error.throw $ ComdirectError "Multiple session object found" Nothing
    [sessObj] -> do
      tanChall <- sessionTanRequest accRefTok sessObj
      printM tanChall
      return (accRefTok, sessObj, tanChall)

secondaryAccessRefreshTokenRequest ::
  ( Members
      '[ HttpRequest,
         Error.Error ComdirectError
       ]
      r
  ) =>
  ComdirectConfig ->
  AccessRefreshToken Password ->
  Sem r (AccessRefreshToken CDSecondary)
secondaryAccessRefreshTokenRequest comdConf accRefTok = do
  let req =
        setRequestHeaders [("accept", "application/json"), ("content-type", "application/x-www-form-urlencoded")] $
          setRequestBodyURLEncoded (secondaryTokenRequestParameters $ secondaryTokenRequestBody accRefTok $ mkTokenRequestBody comdConf) $
            parseRequest_ "POST https://api.comdirect.de/oauth/token"
  request req >>= decodeJSON . getResponseBody

generateSecondaryAccessRefreshTokens ::
  ( Members
      '[ Embed IO,
         HttpRequest,
         ClientRequest,
         Error.Error ComdirectError
       ]
      r
  ) =>
  ComdirectConfig ->
  Sem r (AccessRefreshToken CDSecondary)
generateSecondaryAccessRefreshTokens comdConf = do
  (accRefTok, sessObj, tanChall@(TANChallenge _ tanType mChallenge _)) <- oauthSetup comdConf
  mTan <- case mChallenge of
    Just _challenge -> do
      printM "Please enter TAN:"
      embed $ Just . read . reverse . take 6 . reverse <$> getLine
    Nothing -> return Nothing
  case tanType of
    P_TAN_PUSH -> embed $ threadDelay 10_000_000 -- FIXME check back if comdirect implements route for checking
    _ -> return ()
  _ <- activateSessionTAN tanType accRefTok sessObj tanChall mTan
  secondaryAccessRefreshTokenRequest comdConf accRefTok

generateNewAccessRefreshTokens ::
  ( Members
      '[ HttpRequest,
         ClientRequest,
         Error.Error ComdirectError
       ]
      r
  ) =>
  ComdirectConfig ->
  AccessRefreshToken CDSecondary ->
  Sem r (AccessRefreshToken CDSecondary)
generateNewAccessRefreshTokens comdConf accRefTok = do
  let req =
        setRequestHeaders [("accept", "application/json"), ("content-type", "application/x-www-form-urlencoded")] $
          setRequestBodyURLEncoded (refreshTokenRequestParameters $ refreshTokenRequestBody accRefTok $ mkTokenRequestBody comdConf) $
            parseRequest_ "POST https://api.comdirect.de/oauth/token"
  request req >>= decodeJSON . getResponseBody

-- | Invalidates the current `AccessRefreshToken`
-- invalidateAccessRefreshToken ::
--   Members
--     '[ Token
--      ]
--     r =>
--   Sem r ()
-- invalidateAccessRefreshToken = do
--   accessRefreshToken <- getToken
--   let request =
--         setRequestHeaders
--           [ ("accept", "application/json"),
--             ("content-type", "application/json"),
--             ("Authorization", toAuthorizationParameter accessRefreshToken)
--           ]
--           $ parseRequest_ "DELETE https://api.comdirect.de/oauth/revoke"
--   return ()
