{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Comdirect.Utils
  ( printM,
    strictEncode,
    decodeJSON,
    mapLeft,
  )
where

import Comdirect.Types
import Control.Monad.IO.Class (MonadIO, liftIO)
import Data.Aeson
  ( FromJSON,
    ToJSON,
    Value (Object),
    eitherDecode,
    encode,
    parseJSON,
    withObject,
    (.:?),
  )
import qualified Data.ByteString as BS (ByteString)
import qualified Data.ByteString.Lazy as LBS (ByteString, toStrict)
import Debug.Trace (traceShowId)
import Polysemy
import qualified Polysemy.Error as Error

-- | A generialized version of `print`
printM :: (Show a, MonadIO m) => a -> m ()
printM = liftIO . print

-- | JSON encode to a strict `ByteString`
strictEncode :: (ToJSON a) => a -> BS.ByteString
strictEncode = LBS.toStrict . encode

-- | Result of a comdirect request of type `a`
newtype ComdirectRequestResult a = ComdirectRequestResult {unResult :: Either ComdirectError a}

instance (FromJSON a) => FromJSON (ComdirectRequestResult a) where
  parseJSON v@(Object _) =
    withObject
      "ComdirectRequestResult"
      ( \o ->
          (o .:? "error") >>= \(mErr :: Maybe String) ->
            case mErr of
              Nothing -> ComdirectRequestResult . Right <$> parseJSON v
              Just _ -> ComdirectRequestResult . Left <$> parseJSON v
      )
      v
  parseJSON v = ComdirectRequestResult . Right <$> parseJSON v

mapLeft :: (e -> e') -> Either e a -> Either e' a
mapLeft f (Left x) = Left $ f x
mapLeft _ (Right x) = Right x

decodeJSON ::
  (FromJSON a) =>
  (Members '[Error.Error ComdirectError] r) =>
  LBS.ByteString ->
  Sem r a
decodeJSON s =
  case mapLeft (\e -> traceShowId . ComdirectError ("JSON Parse Error: " <> e) $ Just (show s)) $ eitherDecode s of
    Left err -> Error.throw $ traceShowId err
    Right (ComdirectRequestResult (Left err)) -> Error.throw $ traceShowId err
    Right (ComdirectRequestResult (Right x)) -> return x
