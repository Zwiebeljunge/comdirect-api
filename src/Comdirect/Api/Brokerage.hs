{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}

module Comdirect.Api.Brokerage
  ( getDepots,
    getDepotTransactions,
    getDepotPositions,
    getDepotPosition,
    getInstrument,
    getOrders,
    getOrder,
  )
where

import Comdirect.Api.Types
import Comdirect.Api.Utils
import Comdirect.Types
import Polysemy
import qualified Polysemy.Error as Error

-- | Get all `DepotInfo`'s
getDepots ::
  ( Members
      '[ HttpRequest,
         Error.Error ComdirectError,
         ClientRequest,
         Token
       ]
      r
  ) =>
  Sem r (Multiple DepotInfo)
getDepots = normalApiRequest "/brokerage/clients/user/v3/depots"

-- | Get all `Position`'s for the specified `DepotId`
getDepotPositions ::
  ( Members
      '[ HttpRequest,
         Error.Error ComdirectError,
         ClientRequest,
         Token
       ]
      r
  ) =>
  DepotId ->
  Sem r (Multiple Position)
getDepotPositions depotId = normalApiRequest $ "/brokerage/v3/depots/" <> toParam depotId <> "/positions"

-- | Get the `Position` for the given `DepotId` and `PositionId`
getDepotPosition ::
  ( Members
      '[ HttpRequest,
         Error.Error ComdirectError,
         ClientRequest,
         Token
       ]
      r
  ) =>
  DepotId ->
  PositionId ->
  Sem r Position
getDepotPosition depotId positionId = normalApiRequest $ "/brokerage/v3/depots/" <> toParam depotId <> "/positions/" <> toParam positionId

-- | Get the `Transaction`'s for the given `DepotId`
getDepotTransactions ::
  ( Members
      '[ HttpRequest,
         Error.Error ComdirectError,
         ClientRequest,
         Token
       ]
      r
  ) =>
  DepotId ->
  Sem r (Multiple Transaction)
getDepotTransactions depotId = normalApiRequest $ "/brokerage/v3/depots/" <> toParam depotId <> "/transactions"

-- | Get the `Order`'s for the given `DepotId`
getOrders ::
  ( Members
      '[ HttpRequest,
         Error.Error ComdirectError,
         ClientRequest,
         Token
       ]
      r
  ) =>
  DepotId ->
  Sem r (Multiple Order)
getOrders depotId = normalApiRequest $ "/brokerage/depots/" <> toParam depotId <> "/v3/orders"

-- | Get the `Instrument`'s for the given `InstrumentId`
getInstrument ::
  ( Members
      '[ HttpRequest,
         Error.Error ComdirectError,
         ClientRequest,
         Token
       ]
      r
  ) =>
  InstrumentId ->
  Sem r (Multiple Instrument)
getInstrument instrumentId = normalApiRequest $ "/brokerage/v1/instruments/" <> toParam instrumentId

getOrderDimensions = undefined

getDepotOrders = undefined

-- | Get the `Order` for the given `OrderId`
getOrder ::
  ( Members
      '[ HttpRequest,
         Error.Error ComdirectError,
         ClientRequest,
         Token
       ]
      r
  ) =>
  OrderId ->
  Sem r Order
getOrder orderId = normalApiRequest $ "/brokerage/v3/orders/" <> toParam orderId

prevalidate = undefined

validate = undefined

costindicationExAnte = undefined

orders = undefined

prevalidateChange = undefined

validateChange = undefined

costindicationExAnteChange = undefined

changeOrder = undefined

deleteOrder = undefined

validateQuoteTicket = undefined

quoteTicket = undefined

quote = undefined

getDocuments = undefined

getDocument = undefined

getPredocument = undefined

getAllBalances = undefined
