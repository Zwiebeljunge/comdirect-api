{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}

module Comdirect.Api.Types
  ( WKN,
    ISIN,
    DepotId,
    OrderId,
    AccountId,
    PositionId,
    AccountInfo (..),
    DepotInfo (..),
    Transaction (..),
    Aggregated (..),
    AccountType (..),
    Position (..),
    Balance (..),
    TimedBalance (..),
    Instrument (..),
    InstrumentId (..),
    ToParam (..),
    Order (..),
    Multiple (..),
    DepotTransaction (..),
    BookingStatus (..),
    TransactionDirection (..),
    TransactionType (..),
  )
where

import Data.Aeson
import Data.Time (Day, UTCTime)

newtype WKN = WKN String
  deriving (Show, Eq, Ord)

instance FromJSON WKN where
  parseJSON v = WKN <$> parseJSON v

instance ToParam WKN where
  toParam (WKN wkn) = wkn

newtype ISIN = ISIN String
  deriving (Show, Eq, Ord)

instance FromJSON ISIN where
  parseJSON v = ISIN <$> parseJSON v

instance ToParam ISIN where
  toParam (ISIN isin) = isin

newtype DepotId = DepotId String
  deriving (Show, Eq, Ord)

instance FromJSON DepotId where
  parseJSON v = DepotId <$> parseJSON v

instance ToParam DepotId where
  toParam (DepotId dId) = dId

newtype OrderId = OrderId String
  deriving (Show, Eq, Ord)

instance FromJSON OrderId where
  parseJSON v = OrderId <$> parseJSON v

instance ToParam OrderId where
  toParam (OrderId oId) = oId

newtype AccountId = AccountId String
  deriving (Show, Eq, Ord)

instance FromJSON AccountId where
  parseJSON v = AccountId <$> parseJSON v

instance ToParam AccountId where
  toParam (AccountId aId) = aId

newtype PositionId = PositionId String
  deriving (Show, Eq, Ord)

instance FromJSON PositionId where
  parseJSON v = PositionId <$> parseJSON v

instance ToParam PositionId where
  toParam (PositionId aId) = aId

data AccountInfo = AccountInfo
  { accountId :: AccountId,
    accountDisplayId :: Int,
    currency :: String,
    aClientId :: String,
    accountType :: AccountType,
    iban :: String,
    creditLimit :: CreditLimit,
    balance :: Balance,
    balanceEUR :: Balance,
    availableCashAmount :: Balance,
    availableCashAmountEUR :: Balance
  }
  deriving (Show)

instance FromJSON AccountInfo where
  parseJSON = withObject "AccountInfo" $ \o ->
    AccountInfo
      <$> (o .: "accountId")
      <*> ((o .: "account") >>= (fmap read . flip (.:) "accountDisplayId"))
      <*> ((o .: "account") >>= flip (.:) "currency")
      <*> ((o .: "account") >>= flip (.:) "clientId")
      <*> ((o .: "account") >>= flip (.:) "accountType")
      <*> ((o .: "account") >>= flip (.:) "iban")
      <*> ((o .: "account") >>= flip (.:) "creditLimit")
      <*> (o .: "balance")
      <*> (o .: "balanceEUR")
      <*> (o .: "availableCashAmount")
      <*> (o .: "availableCashAmountEUR")

data Balance = Balance
  { bValue :: Double,
    bUnit :: String
  }
  deriving (Show)

instance FromJSON Balance where
  parseJSON = withObject "Balance" $ \o ->
    Balance
      <$> (read <$> o .: "value")
      <*> (o .: "unit")

data CreditLimit = CreditLimit
  { cValue :: Double,
    cUnit :: String
  }
  deriving (Show)

instance FromJSON CreditLimit where
  parseJSON = withObject "CreditLimit" $ \o ->
    CreditLimit
      <$> (read <$> o .: "value")
      <*> (o .: "unit")

data AccountType = AccountType
  { key :: String,
    text :: String
  }
  deriving (Show)

instance FromJSON AccountType where
  parseJSON = withObject "AccountType" $ \o ->
    AccountType
      <$> (o .: "key")
      <*> (o .: "text")

data DepotInfo = DepotInfo
  { depotId :: DepotId,
    depotDisplayId :: Int,
    dClientId :: String,
    defaultSettlementAccountId :: String,
    settlementAccountIds :: [String]
  }
  deriving (Show)

instance FromJSON DepotInfo where
  parseJSON = withObject "DepotInfo" $ \o ->
    DepotInfo
      <$> (o .: "depotId")
      <*> (read <$> o .: "depotDisplayId")
      <*> (o .: "clientId")
      <*> (o .: "depotId")
      <*> (o .: "settlementAccountIds")

data Transaction = Transaction
  { bookingStatus :: BookingStatus,
    bookingDate :: String,
    businessDate :: String,
    quantity :: Balance,
    instrumentId :: String,
    instrument :: Instrument,
    executionPrice :: Balance,
    transactionValue :: Balance,
    transactionDirection :: String,
    transactionType :: String,
    fxRate :: Maybe String
  }
  deriving (Show)

instance FromJSON Transaction where
  parseJSON = withObject "Transaction" $ \o ->
    Transaction
      <$> (o .: "bookingStatus")
      <*> (o .: "bookingDate")
      <*> (o .: "businessDate")
      <*> (o .: "quantity")
      <*> (o .: "instrumentId")
      <*> (o .: "instrument")
      <*> (o .: "executionPrice")
      <*> (o .: "transactionValue")
      <*> (o .: "transactionDirection")
      <*> (o .: "transactionType")
      <*> (o .: "fxRate")

data Instrument = Instrument
  { id :: String,
    wkn :: WKN,
    isin :: ISIN,
    mnemonic :: String,
    name :: String,
    shortName :: String,
    staticData :: StaticData
  }
  deriving (Show)

instance FromJSON Instrument where
  parseJSON = withObject "Instrument" $ \o ->
    Instrument
      <$> (o .: "instrumentId")
      <*> (o .: "wkn")
      <*> (o .: "isin")
      <*> (o .: "mnemonic")
      <*> (o .: "name")
      <*> (o .: "shortName")
      <*> (o .: "staticData")

data Notation = XXX | XXC | XXM | XXP | XXU
  deriving (Show, Read)

instance FromJSON Notation where
  parseJSON v = read <$> parseJSON v

data StaticData = StaticData
  { notation :: Notation,
    sdCurrency :: String,
    instrumentType :: String,
    priipsRelevant :: Bool,
    kidAvailable :: Bool,
    shippingWaiverRequired :: Bool,
    fundRedemptionLimited :: Bool
  }
  deriving (Show)

instance FromJSON StaticData where
  parseJSON = withObject "StaticData" $ \o ->
    StaticData
      <$> (o .: "notation")
      <*> (o .: "currency")
      <*> (o .: "instrumentType")
      <*> (o .: "priipsRelevant")
      <*> (o .: "kidAvailable")
      <*> (o .: "shippingWaiverRequired")
      <*> (o .: "fundRedemptionLimited")

data Aggregated = Aggregated
  { depot :: DepotInfo,
    prevDayValue :: Balance,
    aCurrentValue :: Balance,
    aPurchaseValue :: Balance,
    aProfitLossPurchaseAbs :: Balance,
    aProfitLossPurchaseRel :: Double,
    aProfitLossPrevDayAbs :: Balance,
    aProfitLossPrevDayRel :: Double
  }
  deriving (Show)

instance FromJSON Aggregated where
  parseJSON = withObject "Aggregated" $ \o ->
    Aggregated
      <$> (o .: "depot")
      <*> (o .: "prevDayValue")
      <*> (o .: "currentValue")
      <*> (o .: "purchaseValue")
      <*> (o .: "profitLossPurchaseAbs")
      <*> (read <$> o .: "profitLossPurchaseRel")
      <*> (o .: "profitLossPrevDayAbs")
      <*> (read <$> o .: "profitLossPrevDayRel")

data Position = Position
  { pDepotId :: DepotId,
    positionId :: PositionId,
    pWkn :: WKN,
    custodyType :: String,
    pQuantity :: Balance,
    availableQuantity :: Balance,
    currentPrice :: TimedBalance,
    purchasePrice :: Balance,
    prevDayPrice :: TimedBalance,
    pCurrentValue :: Balance,
    pPurchaseValue :: Balance,
    pProfitLossPurchaseAbs :: Balance,
    pProfitLossPurchaseRel :: Double,
    pProfitLossPrevDayAbs :: Balance,
    pProfitLossPrevDayRel :: Double,
    pInstrument :: Instrument
  }
  deriving (Show)

instance FromJSON Position where
  parseJSON = withObject "Position" $ \o ->
    Position
      <$> (o .: "depotId")
      <*> (o .: "positionId")
      <*> (o .: "wkn")
      <*> (o .: "custodyType")
      <*> (o .: "quantity")
      <*> (o .: "availableQuantity")
      <*> (o .: "currentPrice")
      <*> (o .: "purchasePrice")
      <*> (o .: "prevDayPrice")
      <*> (o .: "currentValue")
      <*> (o .: "purchaseValue")
      <*> (o .: "profitLossPurchaseAbs")
      <*> (read <$> o .: "profitLossPurchaseRel")
      <*> (o .: "profitLossPrevDayAbs")
      <*> (read <$> o .: "profitLossPrevDayRel")
      <*> (o .: "instrument")

data TimedBalance = TimedBalance
  { price :: Balance,
    priceDateTime :: UTCTime
  }
  deriving (Show)

instance FromJSON TimedBalance where
  parseJSON = withObject "TimedBalance" $ \o ->
    TimedBalance
      <$> (o .: "price")
      <*> (o .: "priceDateTime")

data InstrumentId
  = InstrumentWKN WKN
  | InstrumentISIN ISIN
  | InstrumentMnemonic String
  deriving (Show)

class ToParam p where
  toParam :: p -> String

instance ToParam InstrumentId where
  toParam = \case
    InstrumentWKN wkn -> toParam wkn
    InstrumentISIN isin -> toParam isin
    InstrumentMnemonic m -> m

data Side = BUY | SELL
  deriving (Show)

instance FromJSON Side where
  parseJSON (String "BUY") = pure BUY
  parseJSON (String "SELL") = pure SELL

data Order = Order
  { oDepotId :: OrderId,
    settlementAccountId :: String,
    orderId :: String,
    creationTimestamp :: UTCTime,
    legNumber :: Integer,
    bestEx :: Bool,
    orderType :: String,
    orderStatus :: String,
    subOrders :: [Order],
    side :: Side,
    oInstrumentId :: String,
    quoteTicketId :: String,
    quoteId :: String,
    venueId :: String,
    oQuantity :: Int,
    limitExtension :: String,
    tradingRestriction :: String,
    limit :: String,
    triggerLimit :: String,
    trailingLimitDistAbs :: String,
    trailingLimitDistRel :: String,
    validityType :: String,
    validity :: String,
    openQuantity :: String,
    cancelledQuantiy :: String,
    executedQuantity :: String,
    expectedValue :: String,
    execution :: [Execution]
  }
  deriving (Show)

instance FromJSON Order where
  parseJSON = withObject "Order" $ \o ->
    Order
      <$> (o .: "depotId")
      <*> (o .: "settlementAccountId")
      <*> (o .: "orderId")
      <*> (o .: "creationTimestamp")
      <*> (o .: "legNumber")
      <*> (o .: "bestEx")
      <*> (o .: "orderType")
      <*> (o .: "orderStatus")
      <*> (o .: "subOrders")
      <*> (o .: "side")
      <*> (o .: "instrumentId")
      <*> (o .: "quoteTicketId")
      <*> (o .: "quoteId")
      <*> (o .: "venueId")
      <*> (o .: "quantity")
      <*> (o .: "limitExtension")
      <*> (o .: "tradingRestriction")
      <*> (o .: "limit")
      <*> (o .: "triggerLimit")
      <*> (o .: "trailingLimitDistAbs")
      <*> (o .: "trailingLimitDistRel")
      <*> (o .: "validityType")
      <*> (o .: "validity")
      <*> (o .: "openQuantity")
      <*> (o .: "cancelledQuantiy")
      <*> (o .: "executedQuantity")
      <*> (o .: "expectedValue")
      <*> (o .: "executions")

data Execution = Execution
  { executionId :: String,
    executionNumber :: Integer,
    executionQuantity :: String,
    eExecutionPrice :: String,
    executionTimestamp :: UTCTime
  }
  deriving (Show)

instance FromJSON Execution where
  parseJSON = withObject "Order" $ \o ->
    Execution
      <$> (o .: "executionId")
      <*> (o .: "executionNumber")
      <*> (o .: "executionQuantity")
      <*> (o .: "executionPrice")
      <*> (o .: "executionTimestamp")

newtype Multiple a = Multiple {values :: [a]}
  deriving (Show)

instance Foldable Multiple where
  foldr f b = foldr f b . values

instance (FromJSON a) => FromJSON (Multiple a) where
  parseJSON = withObject "Multiple" $ \o ->
    Multiple <$> (o .: "values")

data BookingStatus = Booked | NotBooked
  deriving (Show)

instance FromJSON BookingStatus where
  parseJSON (String "BOOKED") = pure Booked
  parseJSON (String "NOTBOOKED") = pure NotBooked

data TransactionDirection = In | Out
  deriving (Show)

instance FromJSON TransactionDirection where
  parseJSON (String "IN") = pure In
  parseJSON (String "OUT") = pure Out

data TransactionType
  = Sell
  | Other
  | Buy
  | TransferIn
  | TransferOut
  deriving (Show)

instance FromJSON TransactionType where
  parseJSON (String "SELL") = pure Sell
  parseJSON (String "OTHER") = pure Other
  parseJSON (String "BUY") = pure Buy
  parseJSON (String "TRANSFER_IN") = pure TransferIn
  parseJSON (String "TRANSFER_OUT") = pure TransferOut

data DepotTransaction = DepotTransaction
  { dtTransactionId :: Maybe String,
    dtBookingStatus :: BookingStatus,
    dtBookingDate :: Day,
    dtBusinessDate :: Day,
    dtQuantity :: Balance,
    dtInstrumentId :: String,
    dtInstrument :: Instrument,
    dtExecutionPrice :: Balance,
    dtTransactionValue :: Balance,
    dtTransactionDirection :: TransactionDirection,
    dtTransactionType :: TransactionType
  }
  deriving (Show)

instance FromJSON DepotTransaction where
  parseJSON = withObject "DepotTransaction" $ \o ->
    DepotTransaction
      <$> (o .: "transactionId")
      <*> (o .: "bookingStatus")
      <*> (o .: "bookingDate")
      <*> (o .: "businessDate")
      <*> (o .: "quantity")
      <*> (o .: "instrumentId")
      <*> (o .: "instrument")
      <*> (o .: "executionPrice")
      <*> (o .: "transactionValue")
      <*> (o .: "transactionDirection")
      <*> (o .: "transactionType")
