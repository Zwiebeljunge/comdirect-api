{-# LANGUAGE DataKinds #-}

module Comdirect.Api.Banking
  ( getAccountsBalances,
    getAccountBalances,
    getAccountTransactions,
  )
where

import Comdirect.Api.Types
import Comdirect.Api.Utils
import Comdirect.Types
import Polysemy
import qualified Polysemy.Error as Error

-- | Get all account balances
getAccountsBalances ::
  ( Members
      '[ HttpRequest,
         Error.Error ComdirectError,
         ClientRequest,
         Token
       ]
      r
  ) =>
  Sem r (Multiple AccountInfo)
getAccountsBalances = normalApiRequest "/banking/clients/user/v1/accounts/balances"

-- | Get the balance for the specified `AccountId`
getAccountBalances ::
  ( Members
      '[ HttpRequest,
         Error.Error ComdirectError,
         ClientRequest,
         Token
       ]
      r
  ) =>
  AccountId ->
  Sem r AccountInfo
getAccountBalances accountId = normalApiRequest $ "/banking/v2/accounts/" <> toParam accountId <> "/balances"

-- | Get all transactions for the specified `AccountId`
getAccountTransactions ::
  ( Members
      '[ HttpRequest,
         Error.Error ComdirectError,
         ClientRequest,
         Token
       ]
      r
  ) =>
  AccountId ->
  Sem r (Multiple Transaction)
getAccountTransactions accountId = normalApiRequest $ "/banking/v1/accounts/" <> toParam accountId <> "/transactions"
