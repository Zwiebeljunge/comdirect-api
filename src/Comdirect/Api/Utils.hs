{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}

module Comdirect.Api.Utils
  ( normalApiRequest,
    forMultiple,
  )
where

import Comdirect.Api.Types
import Comdirect.Authentication
import Comdirect.Types
import Comdirect.Utils
import Control.Monad
import Data.Aeson
import Network.HTTP.Simple
import Polysemy
import qualified Polysemy.Error as Error

normalApiRequest ::
  (FromJSON a) =>
  ( Members
      '[ HttpRequest,
         Error.Error ComdirectError,
         ClientRequest,
         Token
       ]
      r
  ) =>
  String ->
  Sem r a
normalApiRequest endpoint = do
  clientRequestId <- getClientRequestId
  accessRefreshToken <- getToken
  let req =
        setRequestHeaders
          [ ("accept", "application/json"),
            ("content-type", "application/json"),
            ("Authorization", toAuthorizationParameter accessRefreshToken),
            ("x-http-request-info", strictEncode clientRequestId)
          ]
          $ parseRequest_ ("GET https://api.comdirect.de/api" <> endpoint)
  request req >>= decodeJSON . getResponseBody

forMultiple :: (Monad m) => [a] -> (a -> m (Multiple b)) -> m [b]
forMultiple xs f = concatMap values <$> forM xs f
