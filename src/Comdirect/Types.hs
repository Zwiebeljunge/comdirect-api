{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeOperators #-}

module Comdirect.Types
  ( ComdirectConfig (..),
    ClientRequestId (ClientRequestId),
    ComdirectError (..),
    CDSecondary,
    AccessRefreshToken (AccessRefreshToken, expiresIn),
    Password,
    SessionObject (identifier),
    TANChallenge (..),
    ChallengeId (ChallengeId),
    TokenRequestBody,
    GrantType,
    TANType (..),
    mkSession,
    mkTokenRequestBody,
    secondaryTokenRequestParameters,
    refreshTokenRequestBody,
    refreshTokenRequestParameters,
    tokenRequestParameters,
    secondaryTokenRequestBody,
    Token,
    getToken,
    runTokenAsReader,
    ClientRequest,
    getClientRequestId,
    runClientRequest,
    HttpRequest,
    request,
    runHttpRequest,
    -- ComdirectM,
  )
where

import Data.Aeson
import qualified Data.ByteString as BS
import Data.ByteString.Char8 (pack)
import qualified Data.ByteString.Lazy as LBS
import Data.Proxy
import Data.Time
import Data.UUID hiding (toString)
import Data.UUID.V4
import Network.HTTP.Client (HttpException)
import Network.HTTP.Simple (Request, Response, httpLBS)
import Polysemy
import qualified Polysemy.Error as Error
import Polysemy.Reader

data ComdirectConfig = ComdirectConfig
  { confUserId :: String,
    confUserSecret :: String,
    confUsername :: String,
    confPassword :: String,
    confPreferedTANMethod :: Maybe TANType
  }

instance Show ComdirectConfig where
  show (ComdirectConfig uId uSec user pass mTanType) =
    "ComdirectConfig { confUserId = "
      <> hideChars uId
      <> ", confUserSecret = "
      <> hideChars uSec
      <> ", confUsername = "
      <> hideChars user
      <> ", confPassword = "
      <> hideChars pass
      <> ", confPreferedTANMethod = "
      <> show mTanType
      <> " }"
    where
      hideChar _ = '*'
      hideChars = map hideChar

data ComdirectError = ComdirectError
  { err :: String,
    errorDescription :: Maybe String
  }
  deriving (Show)

instance FromJSON ComdirectError where
  parseJSON = withObject "ComdirectError" $ \o ->
    ComdirectError
      <$> (o .: "error")
      <*> (o .: "error_description")

data ClientRequestId = ClientRequestId
  { sessionId :: UUID,
    requestId :: String
  }
  deriving (Show)

instance ToJSON ClientRequestId where
  toJSON (ClientRequestId sessId reqId) =
    object
      [ "clientRequestId"
          .= object
            [ "sessionId" .= show sessId,
              "requestId" .= reqId
            ]
      ]

data TokenRequestBody g = TokenRequestBody
  { clientId :: String,
    clientSecret :: String,
    username :: String,
    password :: String
  }

mkTokenRequestBody :: (GrantType t) => ComdirectConfig -> TokenRequestBody t
mkTokenRequestBody (ComdirectConfig userId userSecret user pass _) = TokenRequestBody userId userSecret user pass

data Password

data CDSecondary

-- | Typeclass for types of grants for an `AccessRefreshToken`
class GrantType g where
  toString :: Proxy g -> String

instance GrantType Password where
  toString _ = "password"

instance GrantType CDSecondary where
  toString _ = "cd_secondary"

tokenRequestParameters :: forall t. (GrantType t) => TokenRequestBody t -> [(BS.ByteString, BS.ByteString)]
tokenRequestParameters (TokenRequestBody cId cSecret user pass) =
  [ ("client_id", pack cId),
    ("client_secret", pack cSecret),
    ("username", pack user),
    ("password", pack pass),
    ("grant_type", pack (toString @t Proxy))
  ]

data AccessRefreshToken g = AccessRefreshToken
  { accessToken :: String,
    tokenType :: String,
    refreshToken :: String,
    expiresIn :: Int,
    scope :: String
  }
  deriving (Show)

instance (GrantType g) => FromJSON (AccessRefreshToken g) where
  parseJSON = withObject "AccessRefreshToken" $ \o ->
    AccessRefreshToken
      <$> (o .: "access_token")
      <*> (o .: "token_type")
      <*> (o .: "refresh_token")
      <*> (o .: "expires_in")
      <*> (o .: "scope")

data SessionObject = SessionObject
  { identifier :: String,
    sessionTanActive :: Bool,
    activated2FA :: Bool
  }
  deriving (Show)

instance FromJSON SessionObject where
  parseJSON = withObject "SessionObject" $ \o ->
    SessionObject
      <$> (o .: "identifier")
      <*> (o .: "sessionTanActive")
      <*> (o .: "activated2FA")

instance ToJSON SessionObject where
  toJSON (SessionObject ident sessTanAct act2FA) =
    object
      [ "identifier" .= ident,
        "sessionTanActive" .= sessTanAct,
        "activated2FA" .= act2FA
      ]

mkSession :: SessionObject -> SessionObject
mkSession (SessionObject ident _ _) = SessionObject ident True True

-- | Different TAN methods
data TANType = M_TAN | P_TAN | P_TAN_APP | P_TAN_PUSH
  deriving (Show, Read, Eq)

-- | A TAN challenge
data TANChallenge = TANChallenge
  { challengeId :: ChallengeId,
    typeTAN :: TANType,
    challenge :: Maybe String,
    availableTypes :: [TANType]
  }
  deriving (Show)

instance FromJSON TANChallenge where
  parseJSON = withObject "TANChallenge" $ \o ->
    TANChallenge
      <$> (ChallengeId . read <$> o .: "id")
      <*> (read <$> o .: "typ")
      <*> (o .:? "challenge")
      <*> (map read <$> o .: "availableTypes")

newtype ChallengeId = ChallengeId {unChallengeId :: Int}
  deriving (Show)

instance ToJSON ChallengeId where
  toJSON (ChallengeId i) = object ["id" .= show i]

data SecondaryTokenRequestBody = SecondaryTokenRequestBody
  { sClientId :: String,
    sClientSecret :: String,
    sToken :: String
  }

secondaryTokenRequestBody :: AccessRefreshToken Password -> TokenRequestBody Password -> SecondaryTokenRequestBody
secondaryTokenRequestBody accRefTok trb =
  SecondaryTokenRequestBody
    (clientId trb)
    (clientSecret trb)
    (accessToken accRefTok)

secondaryTokenRequestParameters :: SecondaryTokenRequestBody -> [(BS.ByteString, BS.ByteString)]
secondaryTokenRequestParameters (SecondaryTokenRequestBody cId cSecret tok) =
  [ ("client_id", pack cId),
    ("client_secret", pack cSecret),
    ("token", pack tok),
    ("grant_type", "cd_secondary")
  ]

data RefreshTokenRequestBody = RefreshTokenRequestBody
  { rClientId :: String,
    rClientSecret :: String,
    rToken :: String
  }

refreshTokenRequestBody :: AccessRefreshToken CDSecondary -> TokenRequestBody CDSecondary -> RefreshTokenRequestBody
refreshTokenRequestBody accRefTok trb =
  RefreshTokenRequestBody
    (clientId trb)
    (clientSecret trb)
    (refreshToken accRefTok)

refreshTokenRequestParameters :: RefreshTokenRequestBody -> [(BS.ByteString, BS.ByteString)]
refreshTokenRequestParameters (RefreshTokenRequestBody cId cSecret tok) =
  [ ("client_id", pack cId),
    ("client_secret", pack cSecret),
    ("refresh_token", pack tok),
    ("grant_type", "refresh_token")
  ]

data Token r a where
  GetToken :: Token r (AccessRefreshToken CDSecondary)

makeSem ''Token

runTokenAsReader :: (Members '[Reader (AccessRefreshToken CDSecondary)] r) => Sem (Token : r) a -> Sem r a
runTokenAsReader =
  interpret $ \case
    GetToken -> ask

data ClientRequest r a where
  GetClientRequestId :: ClientRequest r ClientRequestId

makeSem ''ClientRequest

-- | Generate a random `ClientRequestId`
randomClientRequestId :: (Members '[Embed IO] r) => Sem r ClientRequestId
randomClientRequestId = do
  sessId <- embed nextRandom
  reqId <- embed $ formatTime defaultTimeLocale "%H%M%S%3q" <$> getZonedTime
  return $ ClientRequestId sessId reqId

runClientRequest :: (Members '[Embed IO] r) => Sem (ClientRequest : r) a -> Sem r a
runClientRequest =
  interpret $ \case
    GetClientRequestId -> randomClientRequestId

data HttpRequest r a where
  Request :: Request -> HttpRequest r (Response LBS.ByteString)

makeSem ''HttpRequest

runHttpRequest :: (Members '[Embed IO, Error.Error ComdirectError] r) => Sem (HttpRequest : r) a -> Sem r a
runHttpRequest =
  interpret $ \case
    Request req ->
      (\(e :: HttpException) -> ComdirectError "HttpException" (Just $ show e))
        `Error.fromExceptionVia` httpLBS req
